import React, { Component } from 'react';

export default class App extends Component {
  render() {
    return (
      <div>
        <img className="img-fluid react-img" src="./images/image.png" alt=""/>
      <h1>React-redux Starter with Sass and Boostrap v4</h1>
      </div>
    );
  }
}
