import React from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { HashRouter, Route, Switch } from 'react-router-dom';
//usa BrowserRouter en servidores que usan url bonitas(multi-page);

import promise from 'redux-promise';

import reducers from './reducers';

import App from './components/app';

const createStoreWithMiddleware = applyMiddleware(promise)(createStore);
ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
      <HashRouter>
        <div>
          <Switch>
          <Route path="/" component={ App } />
          </Switch>
        </div>
      </HashRouter>
  </Provider>
  , document.querySelector('.container-fluid'));
