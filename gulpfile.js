var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var concatCss = require('gulp-concat-css');
var uglifycss = require('gulp-uglifycss');
var webpack = require('webpack-stream');



gulp.task('jsVendor', function() {
  return gulp.src(['./node_modules/jquery/dist/jquery.min.js', './node_modules/tether/dist/js/tether.min.js', './node_modules/popper.js/dist/umd/popper.min.js', './node_modules/bootstrap/dist/js/bootstrap.min.js'])
      .pipe(uglify())
      .pipe(concat('vendor.js'))
      .pipe(gulp.dest("./js"))
      .pipe(browserSync.stream());
});

gulp.task(
  'sass', ()=>
  gulp.src(['./node_modules/bootstrap/scss/bootstrap.scss', './assets/scss/main.scss'])
  .pipe(sass({outputStyle:'compressed'}).on('error', sass.logError))
  .pipe(autoprefixer({
    versions: ['last 2 browsers']
  }))
  .pipe(concatCss("style.css"))
  .pipe(uglifycss({
    "maxLineLen": 80,
    "uglyComments": true
  }))
  .pipe(gulp.dest('./css'))
  .pipe(browserSync.stream())
);

gulp.task('webpack', function() {
  return gulp.src('./assets/react/index.js')
  .pipe(webpack( require('./webpack.config.js') ))
  .pipe(gulp.dest('./js'));
});


// Static Server + watching scss/html files
gulp.task('serve', ['sass', 'webpack', 'jsVendor'], function(done) {

      browserSync.init({
          server: "./"
      });

      gulp.watch("./assets/scss/*.scss", ['sass']).on('change', browserSync.reload);
      gulp.watch("./index.html").on('change', browserSync.reload);
      gulp.watch("./assets/react/**/*.js", ['webpack']).on('change', browserSync.reload);

      done();
  });

  gulp.task('default', ['jsVendor','webpack', 'sass','serve']);
