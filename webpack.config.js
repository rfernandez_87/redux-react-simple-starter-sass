const webpack = require('webpack'); //to access built-in plugins
const path = require('path');

const config = {
  entry: './assets/react/index.js',
  output: {
    path: path.resolve(__dirname, './js'),
    filename: 'main.js'
  },
  module: {
    loaders: [{
      test: [/\.jsx?$/, /\.js?$/],
      exclude: /node_modules/,
      loader: 'babel-loader',
      query: {
        presets: ['react', 'es2015', 'stage-1']
      }
    }]
  },
  plugins:[
    new webpack.optimize.UglifyJsPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    })
  ],

};

module.exports = config;